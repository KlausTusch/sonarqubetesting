package hello

import "fmt"

func Hello() {
	myNumber := 1
	if myNumber == 1 {
		fmt.Println("Lint me please", myNumber)
	} else if myNumber == 2 {
		fmt.Println("Lint me please", myNumber)
	} else {
		fmt.Println("Lint me please", myNumber)
	}
	if true {
		fmt.Println("need some more lines + 1")
		fmt.Println("need some more lines + 1")
		fmt.Println("need some more lines + 1")
		do1()
	}

	fmt.Println("need some more lines + 1")
	fmt.Println("need some more lines + 1")
	fmt.Println("need some more lines + 1")
	do2()
}

func do1() {
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
}

func do2() {
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
	fmt.Println("need some more lines")
}

func do3() {
	//adding another function that is never called, with a duplication problem
	fmt.Println("How are you doing?")
	fmt.Println("How are you doing?")
	fmt.Println("How are you doing?")
	fmt.Println("How are you doing?")
	fmt.Println("How are you doing?")
}

func returnMyValue() string {
	fmt.Println("Maybe we should do something")
	return "123"
}
