package hello

import "testing"

func TestMainMethod(t *testing.T) {
	result := returnMyValue()
	if result != "123" {
		t.Errorf("Invalid return value expected 123 but got %v", result)
	}
}
