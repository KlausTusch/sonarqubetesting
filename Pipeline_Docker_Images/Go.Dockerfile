FROM golang:1.16.0-buster

ENV PATH="${PATH}:/usr/local/sonar-scanner/bin"

# Install sonar-scanner
RUN apt-get update && apt-get install -y unzip && rm -rf /var/lib/apt/lists/* && \
    wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.6.0.2311-linux.zip && \
    unzip sonar-scanner-cli-4.6.0.2311-linux.zip && mv sonar-scanner-4.6.0.2311-linux /usr/local/sonar-scanner && \
    rm -rf sonar-scanner-cli-4.6.0.2311-linux.zip

# Install golangci lint
RUN curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.38.0